// console.log("Hello Gabby!");

// Mock Database
let posts= [];

//Post ID
let count = 1;

//Add post

document.querySelector('#form-add-post').addEventListener("submit", (e) => {
	e.preventDefault();

	posts.push({
		id: count,
		title:document.querySelector("#txt-title").value,
		body:document.querySelector("#txt-body").value
	});
	count++;
	showPosts();
});


const showPosts = () =>{
	let postsEntries = "";
	posts.forEach((post) => {
		postsEntries += `
			<div id="post-${post.id}">
				<h3 id="post-title-${post.id}">${post.title}</h3>
				<p id="post-body-${post.id}">${post.body}</p>
				<button onclick="editPost(${post.id})">Edit</button>
				<button onclick="deletePost(${post.id})">Delete ${post.id}</button>
			</div>
		`
	});
	// console.log(postsEntries);
	// console.log(posts)

	document.querySelector("#div-post-entries").innerHTML = postsEntries
}


//Edit post

const editPost = (id)=> {
	let title = document.querySelector(`#post-title-${id}`).innerHTML;
	let body = document.querySelector(`#post-body-${id}`).innerHTML;

	document.querySelector('#txt-edit-id').value = id
	document.querySelector('#txt-edit-title').value = title
	document.querySelector('#txt-edit-body').value = body
}

//update post - event listener
document.querySelector('#form-edit-post').addEventListener('submit', (e) => {
	e.preventDefault();

	for(let i = 0 ; i < posts.length; i++){
		if(posts[i].id.toString() === document.querySelector('#txt-edit-id').value){
			posts[i].title =document.querySelector('#txt-edit-title').value;
			posts[i].body =document.querySelector('#txt-edit-body').value;

			showPosts(posts)
			// alert('Successfully Updated!');
			break;
		}

	}

})

const deletePost = (id) => {
 posts.splice(posts.findIndex( e => e.id == id),1);
 document.querySelector(`#post-${id}`).remove();
console.log(posts);
}


// const deletePost = (id) => {
// 	posts = posts.filter((post) => {
// 		if(post.id != id) {
// 			return post
// 		};
// 	})
// 	console.log(posts);

// 	document.querySelector(`#post-${id}`).remove();
// };




// const deletePost = (id) => {
// for(let i = 0; i < posts.length; i++){
// 	if(posts[i].id === id ){
// 		posts.splice(i,1);
// 		console.log(posts[i])
// 	document.querySelector(`#post-${id}`).remove();

// 	showPosts(posts);
// 	break;
// 	}
// }

// }
